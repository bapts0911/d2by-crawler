# d2by-crawler tool

Tool used to get all items over 15$ on dota2bestyolo and corresponding sell link on c5game then export to xls file.

Version 1.1.0

## Changelog

Ver 1.1.0: add text area to display process and using thread.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You will need those following python packages before running the script standalone: xlwt, browsercookie, requests, BeautifulSoup

```
pip install xlwt
pip install browsercookie
pip install requests
pip install BeautifulSoup
```

This tool will take your chrome cookies to get data from dota2bestyolo so make sure you logged on this site by chrome and reach the addtrade page.

### Installing

For fast running script without select destination location:

```
python .\d2by_crawler.py
```

For open a GUI to select destination location:

```
python .\d2by_crawler_gui.py
```

Pick the folder you want to save and process.

The result file's name is : data_fillted-for-D2BY.xls

The file result will have following format:

| Items name           | Link on c5game                           | Value d2by |
| -------------------- | ---------------------------------------- | ---------- |
| Shattered Greatsword | https://www.c5game.com/dota/18894-S.html | 450.92     |
| Dragonclaw Hook      | https://www.c5game.com/dota/5567-S.html  | 400        |
| Pipe of Dezun        | https://www.c5game.com/dota/18950-S.html | 190        |


## Deployment

If you want to deploy to machine which not have python you can build it to executable file.

For window user, you can use PyInstaller:

```
pyinstaller d2by_crawler_gui.spec
```

## Built With

* [Requests](http://docs.python-requests.org/en/master/) - The requests framework used
* [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/) - The html parser
* [Browsercookie](https://pypi.python.org/pypi/browsercookie/) - Used to get browser cookie
* [Xlwt](https://pypi.python.org/pypi/xlwt) - Used to create excel result file

## Contributing

Yami

## Versioning

We use [SemVer](http://semver.org/) for versioning. 

## Authors

* **[Yami](https://facebook.com/son.thien.12)** - *Project owner*

## License

I don't know what is license of this project

## Acknowledgments

* This is my first python project.
* I know my code was sucked. Need more refining and bug fixing later
* This project earn for me 2 arcana FeelsGoodMan
