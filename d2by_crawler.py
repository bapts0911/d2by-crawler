import re
import urllib2
import json
import xlwt
import time
import os
import browsercookie
import requests
from bs4 import BeautifulSoup
from datetime import datetime

def crawl(dir = None, TextArea = None, root = None):
	start_time = time.time()
	excel_filename = '\data_fillted-for-D2BY.xls'
	dir = os.getcwd() if dir is None else dir
	excel_filename = dir + excel_filename
	addtrade_url = 'https://dota2bestyolo.com/trade/addtrade'
	searchitemdota2_url = 'https://dota2bestyolo.com/trade/searchItemDota2'
	searchitemdota2_header = {'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	c5game_search_url = 'https://www.c5game.com/dota'
	c5game_url = 'https://www.c5game.com'

	# create workbook
	book = xlwt.Workbook()
	sheet = book.add_sheet('sheet1')
	show_message('Folder to save: %s' % excel_filename, TextArea, root)
	try:
		book.save(excel_filename)
	except IOError:
		show_message('Close your fucking file before do anything', TextArea, root)
		show_message('Stop!!!', TextArea, root)
		return

	show_message("Get chrome cookies...", TextArea, root)
	cj = browsercookie.chrome()

	# get html from d2by addtrade site
	try:
		show_message("Crawling the d2by/addtrade site...", TextArea, root)
		r = requests.get(addtrade_url, cookies=cj)
	except requests.ConnectionError:
		show_message('Connection error', TextArea, root)
		show_message('Stop!!!', TextArea, root)
		return

	# using beautifulSoup to get csrf token
	show_message("Get csrf token...", TextArea, root)
	parser = BeautifulSoup(r.content, 'html.parser')
	csrf = parser.find(attrs={"type":"hidden","name":"_csrf"})

	# get 2 page item of d2by
	try:
		show_message("Crawling the d2by/searchItemDota2 site...", TextArea, root)
		payload1 = {'name':'', 'hero':'', 'rarity':'', 'quality':'', 'page':'1', '_csrf': csrf['value']}
		r1 = requests.post(searchitemdota2_url, headers=searchitemdota2_header, data=payload1, cookies=cj)

		payload2 = {'name':'', 'hero':'', 'rarity':'', 'quality':'', 'page':'2', '_csrf': csrf['value']}
		r2 = requests.post(searchitemdota2_url, headers=searchitemdota2_header, data=payload2, cookies=cj)
	except requests.ConnectionError:
		show_message('Connection error', TextArea, root)
		show_message('Stop!!!', TextArea, root)
		return

	dict = r1.json()
	parser = BeautifulSoup(dict['data'], 'html.parser')
	tooltips = parser.find_all(attrs={"class":"tooltip-info"})

	dict = r2.json()
	parser = BeautifulSoup(dict['data'], 'html.parser')
	tooltips.extend(parser.find_all(attrs={"class":"tooltip-info"}))

	col1_name = 'Items Name'
	col2_name = 'Link on C5game'
	col3_name = 'Value d2by'
	header_style = xlwt.easyxf('font: bold on')
	sheet.write(0, 0 , col1_name, header_style)
	sheet.write(0, 1 , col2_name, header_style)
	sheet.write(0, 2 , col3_name, header_style)
	sheet.col(0).width = 256*50 # 50 characters
	sheet.col(1).width = 256*50 # 50 characters
	sheet.col(2).width = 256*20 # 20 characters
	n = 1

	for item in tooltips:
		name = item.find(attrs={"class":"tooltip-name text-white text-center"}).string
		p_tag = item.find_all('p')
		status = p_tag[1].string
		regex = re.compile('(\d+(?:\.\d{1,2})?)')
		price = re.search(regex ,p_tag[2].string).group(0)
		if float(price) < 15:
			break

		# get html from c5game site
		message = "Getting the c5game %s link..." %name
		show_message(message, TextArea, root)
		try:
			payload = {'k':name}
			link = c5game_url
			r = requests.get(c5game_search_url, params=payload)
			parser = BeautifulSoup(r.content, 'html.parser')
			selling = parser.find_all('li', class_="selling")
			for item in selling:
				img_tag = item.find('img', attrs={"alt":name})
				if img_tag is not None:
					a_tag = img_tag.find_parent()
					link += a_tag['href']
					break

		except requests.ConnectionError:
			link = 'Connection error'

		if link != c5game_url:
			sheet.write(n, 0, name)
			sheet.write(n, 1, link)
			sheet.write(n, 2, price)
			n += 1

	book.save(excel_filename)
	show_message("Done!!!", TextArea, root)
	show_message("--- Time elapsed: %s seconds ---" % (time.time() - start_time), TextArea, root)

def show_message(message, TextArea=None, root=None):
	if TextArea and root:
		TextArea.insert(0.0,message + '\n')
		root.update_idletasks()
	else:
		print message

if __name__ == '__main__':
    crawl()