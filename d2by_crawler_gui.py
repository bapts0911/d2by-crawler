import Tkinter, tkFileDialog, Tkconstants
import os, errno, thread
import d2by_crawler
from Tkinter import * 

root = Tk() 
root.title('Dota2BestYolo tool ver 1.0')
root.geometry("580x350")
content = Frame(root)
content.grid()

# Directory text value
E = Entry(content, width=50)
E.grid(row=0, padx=40, column=0, sticky =W)
#E.pack()
cur_dir = os.getcwd()
E.insert(END, cur_dir)

# Define asking directory button
dirname = ''
def openDirectory():
	dirname = tkFileDialog.askdirectory(parent=root, initialdir='~', title='Select folder to save')
	E.delete(0, len(E.get()))
	E.insert(0, dirname)
	
select_button = Button(content, text = 'Select folder', width = 15, command= openDirectory)
select_button.grid(row=0, column=1, sticky=W)
#B.pack(**button_opt)
# Action button
def work():
	dir = E.get()
	if not os.path.exists(dir):
		try:
			os.makedirs(dir)
		except OSError as e:
			if e.errno != errno.EEXIST:
				raise
				
	d2by_crawler.crawl(dir, TextArea, root)
	TextArea.insert(0.0,"**********************************************************************" + '\n')
	TextArea.insert(0.0,"************           I'm actually done                  ************" + '\n')
	TextArea.insert(0.0,"************           Get me out of here bitch!          ************" + '\n')
	TextArea.insert(0.0,"**********************************************************************" + '\n')

def thread_process():
	thread.start_new_thread(work, ())

run_button = Button(content, text = 'Action', width = 20, command= thread_process)
run_button.grid(row=3, column=0, padx = 200, columnspan = 2, sticky=W)

TextArea = Text(content, width = 70, height = 17, wrap = WORD)
TextArea.grid(row = 5, padx = 6,column=0, columnspan = 2, sticky = W)
TextArea.insert(END,"**********************************************************************" + '\n')
TextArea.insert(END,"************                    USAGE                     ************" + '\n')
TextArea.insert(END,"************         1. Select destination folder         ************" + '\n')
TextArea.insert(END,"************         2. Who's that handsome devil?        ************" + '\n')
TextArea.insert(END,"**********************************************************************" + '\n')

root.mainloop()